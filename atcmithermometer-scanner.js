const Scanner = require("./Scanner");

module.exports = function (RED) {
  function ATCMiThermometer(config) {
    RED.nodes.createNode(this, config);
    const node = this;

    const scanner = new Scanner();

    let _lastInputMsg;

    node.on("input", function (msg) {
      _lastInputMsg = msg;

      if (msg.topic === "start") {
        scanner.start();
      } else if (msg.topic === "stop") {
        scanner.stop();
      }

      // msg.payload = msg.payload.toLowerCase();
      // node.send(msg);
    });

    node.on("close", function () {
      scanner.close();
      node.status({});
    });

    scanner.on("start", () => {
      node.status({
        fill: "green",
        shape: "dot",
        text: "Scanning..."
      });
    });
    scanner.on("stop", () => {
      node.status({});
    });

    scanner.on("discover", ad => {
      node.send({
        ..._lastInputMsg,
        payload: { ..._lastInputMsg?.payload, ...ad }
      });
    });
  }

  RED.nodes.registerType("atcmithermometer-scanner", ATCMiThermometer);
};

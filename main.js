const Scanner = new require("./Scanner.js");

const scanner = new Scanner({
  rescanDelay: 5 * 1000
});

scanner.on("discover", ad => {
  console.log(
    ad.rssi,
    ad.localName,
    ad.address,
    "\tt",
    ad.temperatureC,
    " h",
    ad.humidity,
    " b",
    ad.battery,
    " b",
    ad.batteryV
  );
});

setInterval(() => {
  scanner.start();
}, 5000);

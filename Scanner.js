const noble = require("@abandonware/noble");
const EventEmitter = require("events");

// Listen on bluetooth advertisement data from Xiaomi's thermometer
// with custom flash: https://github.com/atc1441/ATC_MiThermometer#advertising-format-of-the-custom-firmware

class Scanner extends EventEmitter {
  constructor() {
    super();

    noble.on("stateChange", state => {
      this.emit("stateChange", ...arguments);
      // console.log("stateChange", JSON.stringify(state, 0, 2));
    });

    noble.on("scanStart", () => {
      this.emit("scanStart", ...arguments);
      // console.log(`scanStart`);
    });

    noble.on("scanStop", () => {
      this.emit("scanStop", ...arguments);
      // console.log(`scanStop`);
      // noble._bindings._hci.reset.toString();
    });

    noble.on("discover", p => this.onDiscover(p));
  }

  onDiscover(peripheral) {
    // Only for Xiaomi's thermometer
    if (!peripheral.address.startsWith("a4:c1:38:")) return;

    const buffer = peripheral.advertisement?.serviceData?.find(
      svc => svc.uuid === "181a"
    )?.data;

    if (!buffer) {
      console.error(Object.keys(peripheral));
      console.error(JSON.stringify(peripheral.advertisement, 0, 2));
      noble.stopScanning();
    }

    const { id, rssi, address } = peripheral;
    const { localName } = peripheral.advertisement;

    const output = {
      id,
      rssi,
      address,
      localName,
      temperature: buffer.readUIntBE(6, 2) / 10,
      humidity: buffer.readUIntBE(8, 1),
      battery: buffer.readUIntBE(9, 1),
      batteryV: buffer.readUIntBE(10, 2) / 1000
    };

    this.emit("discover", output);

    noble.stopScanning();

    return output;
  }

  start() {
    if (noble.state !== "poweredOn")
      noble.once("stateChange", state => {
        if (state === "poweredOn") {
          this.start();
        }
      });
    else
      noble.startScanning(["181a"], false, e => {
        e && console.error("error", e);
      });
  }

  stop() {
    noble.stopScanning();
  }
}

module.exports = Scanner;
